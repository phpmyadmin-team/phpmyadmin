��K9      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�User management�h]�h	�Text����User management�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�J/home/ibennetch/svn/phpmyadmin/release/phpMyAdmin-5.2.2/doc/privileges.rst�hKubh	�	paragraph���)��}�(hX�  User management is the process of controlling which users are allowed to
connect to the MySQL server and what permissions they have on each database.
phpMyAdmin does not handle user management, rather it passes the username and
password on to MySQL, which then determines whether a user is permitted to
perform a particular action. Within phpMyAdmin, administrators have full
control over creating users, viewing and editing privileges for existing users,
and removing users.�h]�hX�  User management is the process of controlling which users are allowed to
connect to the MySQL server and what permissions they have on each database.
phpMyAdmin does not handle user management, rather it passes the username and
password on to MySQL, which then determines whether a user is permitted to
perform a particular action. Within phpMyAdmin, administrators have full
control over creating users, viewing and editing privileges for existing users,
and removing users.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh.)��}�(h��Within phpMyAdmin, user management is controlled via the :guilabel:`User accounts` tab
from the main page. Users can be created, edited, and removed.�h]�(h�9Within phpMyAdmin, user management is controlled via the �����}�(hh=hhhNhNubh	�inline���)��}�(h�User accounts�h]�h�User accounts�����}�(hhGhhhNhNubah}�(h!]�h#]��guilabel�ah%]�h']�h)]��rawtext��:guilabel:`User accounts`�uh+hEhh=ubh�C tab
from the main page. Users can be created, edited, and removed.�����}�(hh=hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�Creating a new user�h]�h�Creating a new user�����}�(hhehhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhbhhhh,hKubh.)��}�(hXn  To create a new user, click the :guilabel:`Add user account` link near the bottom
of the :guilabel:`User accounts` page (you must be a "superuser", e.g., user "root").
Use the textboxes and drop-downs to configure the user to your particular
needs. You can then select whether to create a database for that user and grant
specific global privileges. Once you've created the user (by clicking Go), you
can define that user's permissions on a specific database (don't grant global
privileges in that case). In general, users do not need any global privileges
(other than USAGE), only permissions for their specific database.�h]�(h� To create a new user, click the �����}�(hhshhhNhNubhF)��}�(h�Add user account�h]�h�Add user account�����}�(hh{hhhNhNubah}�(h!]�h#]��guilabel�ah%]�h']�h)]��rawtext��:guilabel:`Add user account`�uh+hEhhsubh� link near the bottom
of the �����}�(hhshhhNhNubhF)��}�(h�User accounts�h]�h�User accounts�����}�(hh�hhhNhNubah}�(h!]�h#]��guilabel�ah%]�h']�h)]��rawtext��:guilabel:`User accounts`�uh+hEhhsubhX
   page (you must be a “superuser”, e.g., user “root”).
Use the textboxes and drop-downs to configure the user to your particular
needs. You can then select whether to create a database for that user and grant
specific global privileges. Once you’ve created the user (by clicking Go), you
can define that user’s permissions on a specific database (don’t grant global
privileges in that case). In general, users do not need any global privileges
(other than USAGE), only permissions for their specific database.�����}�(hhshhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhbhhubeh}�(h!]��creating-a-new-user�ah#]�h%]��creating a new user�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Editing an existing user�h]�h�Editing an existing user�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubh.)��}�(h��To edit an existing user, simply click the pencil icon to the right of that
user in the :guilabel:`User accounts` page. You can then edit their global- and
database-specific privileges, change their password, or even copy those
privileges to a new user.�h]�(h�XTo edit an existing user, simply click the pencil icon to the right of that
user in the �����}�(hh�hhhNhNubhF)��}�(h�User accounts�h]�h�User accounts�����}�(hh�hhhNhNubah}�(h!]�h#]��guilabel�ah%]�h']�h)]��rawtext��:guilabel:`User accounts`�uh+hEhh�ubh�� page. You can then edit their global- and
database-specific privileges, change their password, or even copy those
privileges to a new user.�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�hhubeh}�(h!]��editing-an-existing-user�ah#]�h%]��editing an existing user�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Deleting a user�h]�h�Deleting a user�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hK$ubh.)��}�(h��From the :guilabel:`User accounts` page, check the checkbox for the user you wish to
remove, select whether or not to also remove any databases of the same name (if
they exist), and click Go.�h]�(h�	From the �����}�(hj   hhhNhNubhF)��}�(h�User accounts�h]�h�User accounts�����}�(hj  hhhNhNubah}�(h!]�h#]��guilabel�ah%]�h']�h)]��rawtext��:guilabel:`User accounts`�uh+hEhj   ubh�� page, check the checkbox for the user you wish to
remove, select whether or not to also remove any databases of the same name (if
they exist), and click Go.�����}�(hj   hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK&hh�hhubeh}�(h!]��deleting-a-user�ah#]�h%]��deleting a user�ah']�h)]�uh+h
hhhhhh,hK$ubh)��}�(hhh]�(h)��}�(h�4Assigning privileges to user for a specific database�h]�h�4Assigning privileges to user for a specific database�����}�(hj.  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj+  hhhh,hK+ubh.)��}�(hXQ  Users are assigned to databases by editing the user record (from the
:guilabel:`User accounts` link on the home page).
If you are creating a user specifically for a given table
you will have to create the user first (with no global privileges) and then go
back and edit that user to add the table and privileges for the individual
table.�h]�(h�EUsers are assigned to databases by editing the user record (from the
�����}�(hj<  hhhNhNubhF)��}�(h�User accounts�h]�h�User accounts�����}�(hjD  hhhNhNubah}�(h!]�h#]��guilabel�ah%]�h']�h)]��rawtext��:guilabel:`User accounts`�uh+hEhj<  ubh�� link on the home page).
If you are creating a user specifically for a given table
you will have to create the user first (with no global privileges) and then go
back and edit that user to add the table and privileges for the individual
table.�����}�(hj<  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK-hj+  hhubh	�target���)��}�(h�.. _configurablemenus:�h]�h}�(h!]�h#]�h%]�h']�h)]��refid��configurablemenus�uh+j_  hK4hj+  hhhh,ubeh}�(h!]��4assigning-privileges-to-user-for-a-specific-database�ah#]�h%]��4assigning privileges to user for a specific database�ah']�h)]�uh+h
hhhhhh,hK+ubh)��}�(hhh]�(h)��}�(h�"Configurable menus and user groups�h]�h�"Configurable menus and user groups�����}�(hjx  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhju  hhhh,hK7ubh.)��}�(h��By enabling :config:option:`$cfg['Servers'][$i]['users']` and
:config:option:`$cfg['Servers'][$i]['usergroups']` you can customize what users
will see in the phpMyAdmin navigation.�h]�(h�By enabling �����}�(hj�  hhhNhNubh �index���)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]��entries�]�((�single��$cfg['Servers'][$i]['users']��index-0�j�  Nt�(j�  �2configuration option; $cfg['Servers'][$i]['users']�j�  j�  Nt�euh+j�  hj�  ubj`  )��}�(hhh]�h}�(h!]�j�  ah#]�h%]�h']�h)]�uh+j_  hj�  ubh �pending_xref���)��}�(h�-:config:option:`$cfg['Servers'][$i]['users']`�h]�h	�literal���)��}�(hj�  h]�h�$cfg['Servers'][$i]['users']�����}�(hj�  hhhNhNubah}�(h!]�h#]�(�xref��config��config-option�eh%]�h']�h)]�uh+j�  hj�  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc��
privileges��	refdomain�j�  �reftype��option��refexplicit���refwarn���	reftarget�j�  uh+j�  hh,hK9hj�  ubh� and
�����}�(hj�  hhhNhNubj�  )��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�j�  ]�((j�  �!$cfg['Servers'][$i]['usergroups']��index-1�j�  Nt�(j�  �7configuration option; $cfg['Servers'][$i]['usergroups']�j�  j�  Nt�euh+j�  hj�  ubj`  )��}�(hhh]�h}�(h!]�j�  ah#]�h%]�h']�h)]�uh+j_  hj�  ubj�  )��}�(h�2:config:option:`$cfg['Servers'][$i]['usergroups']`�h]�j�  )��}�(hj�  h]�h�!$cfg['Servers'][$i]['usergroups']�����}�(hj�  hhhNhNubah}�(h!]�h#]�(j�  �config��config-option�eh%]�h']�h)]�uh+j�  hj�  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc�j�  �	refdomain�j�  �reftype��option��refexplicit���refwarn��j�  j�  uh+j�  hh,hK9hj�  ubh�D you can customize what users
will see in the phpMyAdmin navigation.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK9hju  hhubh	�warning���)��}�(h��This feature only limits what a user sees, they are still able to use all the
functions. So this can not be considered as a security limitation. Should
you want to limit what users can do, use MySQL privileges to achieve that.�h]�h.)��}�(h��This feature only limits what a user sees, they are still able to use all the
functions. So this can not be considered as a security limitation. Should
you want to limit what users can do, use MySQL privileges to achieve that.�h]�h��This feature only limits what a user sees, they are still able to use all the
functions. So this can not be considered as a security limitation. Should
you want to limit what users can do, use MySQL privileges to achieve that.�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK?hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j  hju  hhhh,hNubh.)��}�(hX�  With this feature enabled, the :guilabel:`User accounts` management interface gains
a second tab for managing :guilabel:`User groups`, where you can define what each
group will view (see image below) and you can then assign each user to one of
these groups. Users will be presented with a simplified user interface, which might be
useful for inexperienced users who could be overwhelmed by all the features
phpMyAdmin provides.�h]�(h�With this feature enabled, the �����}�(hj/  hhhNhNubhF)��}�(h�User accounts�h]�h�User accounts�����}�(hj7  hhhNhNubah}�(h!]�h#]��guilabel�ah%]�h']�h)]��rawtext��:guilabel:`User accounts`�uh+hEhj/  ubh�6 management interface gains
a second tab for managing �����}�(hj/  hhhNhNubhF)��}�(h�User groups�h]�h�User groups�����}�(hjL  hhhNhNubah}�(h!]�h#]��guilabel�ah%]�h']�h)]��rawtext��:guilabel:`User groups`�uh+hEhj/  ubhX&  , where you can define what each
group will view (see image below) and you can then assign each user to one of
these groups. Users will be presented with a simplified user interface, which might be
useful for inexperienced users who could be overwhelmed by all the features
phpMyAdmin provides.�����}�(hj/  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKChju  hhubh	�image���)��}�(h� .. image:: images/usergroups.png�h]�h}�(h!]�h#]�h%]�h']�h)]��uri��images/usergroups.png��
candidates�}��*�jt  suh+jg  hju  hhhh,hKJubeh}�(h!]�(�"configurable-menus-and-user-groups�jl  eh#]�h%]�(�"configurable menus and user groups��configurablemenus�eh']�h)]�uh+h
hhhhhh,hK7�expect_referenced_by_name�}�j~  ja  s�expect_referenced_by_id�}�jl  ja  subeh}�(h!]��user-management�ah#]�h%]��user management�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�jl  ]�ja  as�nameids�}�(j�  j�  h�h�h�h�j(  j%  jr  jo  j~  jl  j}  jz  u�	nametypes�}�(j�  �h��h�j(  �jr  �j~  �j}  �uh!}�(j�  hh�hbh�h�j%  h�jo  j+  jl  ju  jz  ju  j�  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]�(h	�system_message���)��}�(hhh]�h.)��}�(hhh]�h�7Hyperlink target "configurablemenus" is not referenced.�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�uh+h-hj  ubah}�(h!]�h#]�h%]�h']�h)]��level�K�type��INFO��source�h,�line�K4uh+j  ubj  )��}�(hhh]�h.)��}�(hhh]�h�-Hyperlink target "index-0" is not referenced.�����}�hj4  sbah}�(h!]�h#]�h%]�h']�h)]�uh+h-hj1  ubah}�(h!]�h#]�h%]�h']�h)]��level�K�type�j.  �source�h,�line�K9uh+j  ubj  )��}�(hhh]�h.)��}�(hhh]�h�-Hyperlink target "index-1" is not referenced.�����}�hjN  sbah}�(h!]�h#]�h%]�h']�h)]�uh+h-hjK  ubah}�(h!]�h#]�h%]�h']�h)]��level�K�type�j.  �source�h,�line�K9uh+j  ube�transformer�N�include_log�]��
decoration�Nhhub.