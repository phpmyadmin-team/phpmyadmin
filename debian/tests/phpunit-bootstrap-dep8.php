<?php

if (! defined('ROOT_PATH')) {
    define('ROOT_PATH', '/usr/share/phpmyadmin/');
}

if (! defined('TEST_PATH')) {
    define('TEST_PATH', __DIR__ . '/../../');
}

require_once TEST_PATH . 'test/autoload.php';// Load test libraires
require_once TEST_PATH . 'test/bootstrap-dist.php';// Resume normal loading
