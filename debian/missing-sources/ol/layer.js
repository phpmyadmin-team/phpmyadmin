/**
 * @module ol/layer
 */
export { default as Group } from './layer/Group.js';
export { default as Image } from './layer/Image.js';
export { default as Layer } from './layer/Layer.js';
export { default as Tile } from './layer/Tile.js';
export { default as Vector } from './layer/Vector.js';
//# sourceMappingURL=layer.js.map
