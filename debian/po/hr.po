# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the phpmyadmin package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: phpmyadmin\n"
"Report-Msgid-Bugs-To: phpmyadmin@packages.debian.org\n"
"POT-Creation-Date: 2019-10-22 22:04+0200\n"
"PO-Revision-Date: 2023-12-08 16:07+0000\n"
"Last-Translator: Milo Ivir <mail@milotype.de>\n"
"Language-Team: Croatian <https://hosted.weblate.org/projects/phpmyadmin/"
"debian/hr/>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 5.3-dev\n"

#. Type: multiselect
#. Description
#: ../templates:2001
msgid "Web server to reconfigure automatically:"
msgstr "Web poslužitelj za automatsku konfiguraciju:"

#. Type: multiselect
#. Description
#: ../templates:2001
msgid ""
"Please choose the web server that should be automatically configured to run "
"phpMyAdmin."
msgstr ""
"Odaberite web poslužitelj koji bi se morao automatski konfigurirati za "
"pokretanje phpMyAdmin."
