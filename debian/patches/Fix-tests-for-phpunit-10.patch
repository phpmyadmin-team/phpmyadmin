From: William Desportes <williamdes@wdes.fr>
Date: Tue, 26 Sep 2023 14:40:45 +0200
Subject: Fix tests for phpunit 10

Ref: https://github.com/sebastianbergmann/phpunit/issues/5062#issuecomment-1446620312

Origin: vendor
Forwarded: not-needed
---
 test/classes/ConfigTest.php                   | 13 +++++++++++--
 test/classes/ErrorTest.php                    |  9 +++++++++
 test/classes/Navigation/NodeFactoryTest.php   | 25 +++++++++++++++++++++----
 test/classes/Plugins/Export/ExportOdsTest.php | 13 ++++++++++---
 test/classes/Plugins/Export/ExportOdtTest.php | 13 ++++++++++---
 5 files changed, 61 insertions(+), 12 deletions(-)

diff --git a/test/classes/ConfigTest.php b/test/classes/ConfigTest.php
index acfd4cc..b9ada1e 100644
--- a/test/classes/ConfigTest.php
+++ b/test/classes/ConfigTest.php
@@ -7,6 +7,7 @@ namespace PhpMyAdmin\Tests;
 use PhpMyAdmin\Config;
 use PhpMyAdmin\Config\Settings;
 use PhpMyAdmin\DatabaseInterface;
+use Exception;
 
 use function array_merge;
 use function array_replace_recursive;
@@ -1074,8 +1075,16 @@ class ConfigTest extends AbstractTestCase
      */
     public function testCheckServersWithInvalidServer(): void
     {
-        $this->expectError();
-        $this->expectErrorMessage('Invalid server index: invalid');
+        set_error_handler(
+            static function ($errno, $errstr) {
+                restore_error_handler();
+                throw new Exception($errstr, $errno);
+            },
+            E_ALL
+        );
+
+        $this->expectException(Exception::class);
+        $this->expectExceptionMessage('Invalid server index: invalid');
 
         $this->object->settings['Servers'] = ['invalid' => ['host' => '127.0.0.1'], 1 => ['host' => '127.0.0.1']];
         $this->object->checkServers();
diff --git a/test/classes/ErrorTest.php b/test/classes/ErrorTest.php
index d78a216..ac0e740 100644
--- a/test/classes/ErrorTest.php
+++ b/test/classes/ErrorTest.php
@@ -132,6 +132,15 @@ class ErrorTest extends AbstractTestCase
      */
     public function testGetBacktraceDisplay(): void
     {
+        // phpunit >= 10
+        if (class_exists(\PHPUnit\Framework\TestRunner::class)) {
+            self::assertStringContainsString(
+                'PHPUnit\Framework\TestRunner->run(<Class:PhpMyAdmin\Tests\ErrorTest>)<br>',
+                $this->object->getBacktraceDisplay()
+            );
+            return;
+        }
+
         self::assertStringContainsString(
             'PHPUnit\Framework\TestResult->run(<Class:PhpMyAdmin\Tests\ErrorTest>)<br>',
             $this->object->getBacktraceDisplay()
diff --git a/test/classes/Navigation/NodeFactoryTest.php b/test/classes/Navigation/NodeFactoryTest.php
index fd746a7..9d5b18e 100644
--- a/test/classes/Navigation/NodeFactoryTest.php
+++ b/test/classes/Navigation/NodeFactoryTest.php
@@ -7,6 +7,7 @@ namespace PhpMyAdmin\Tests\Navigation;
 use PhpMyAdmin\Navigation\NodeFactory;
 use PhpMyAdmin\Navigation\Nodes\Node;
 use PhpMyAdmin\Tests\AbstractTestCase;
+use Exception;
 
 /**
  * @covers \PhpMyAdmin\Navigation\NodeFactory
@@ -52,8 +53,16 @@ class NodeFactoryTest extends AbstractTestCase
      */
     public function testFileError(): void
     {
-        $this->expectError();
-        $this->expectErrorMessage('Could not load class "PhpMyAdmin\Navigation\Nodes\Node"');
+        set_error_handler(
+            static function ($errno, $errstr) {
+                restore_error_handler();
+                throw new Exception($errstr, $errno);
+            },
+            E_ALL
+        );
+
+        $this->expectException(Exception::class);
+        $this->expectExceptionMessage('Could not load class "PhpMyAdmin\Navigation\Nodes\Node"');
         NodeFactory::getInstance('NodeDoesNotExist');
     }
 
@@ -63,8 +72,16 @@ class NodeFactoryTest extends AbstractTestCase
      */
     public function testClassNameError(): void
     {
-        $this->expectError();
-        $this->expectErrorMessage('Invalid class name "Node", using default of "Node"');
+        set_error_handler(
+            static function ($errno, $errstr) {
+                restore_error_handler();
+                throw new Exception($errstr, $errno);
+            },
+            E_ALL
+        );
+
+        $this->expectException(Exception::class);
+        $this->expectExceptionMessage('Invalid class name "Node", using default of "Node"');
         NodeFactory::getInstance('Invalid');
     }
 }
diff --git a/test/classes/Plugins/Export/ExportOdsTest.php b/test/classes/Plugins/Export/ExportOdsTest.php
index fef9bf2..3fd0b0a 100644
--- a/test/classes/Plugins/Export/ExportOdsTest.php
+++ b/test/classes/Plugins/Export/ExportOdsTest.php
@@ -137,10 +137,17 @@ class ExportOdsTest extends AbstractTestCase
     {
         $GLOBALS['ods_buffer'] = 'header';
 
-        $this->expectOutputRegex('/^504b.*636f6e74656e742e786d6c/');
-        $this->setOutputCallback('bin2hex');
+        \ob_start();
+        $this->assertTrue(
+            $this->object->exportFooter()
+        );
+        $output = \ob_get_contents();
+        \ob_end_clean();
 
-        self::assertTrue($this->object->exportFooter());
+        $this->assertStringContainsString('content.xml', $output);
+        $output = bin2hex($output);
+        // ODT, ODS, ... file header
+        $this->assertSame('504b0304', substr($output, 0, 8));
 
         self::assertStringContainsString('header', $GLOBALS['ods_buffer']);
 
diff --git a/test/classes/Plugins/Export/ExportOdtTest.php b/test/classes/Plugins/Export/ExportOdtTest.php
index 7bfc47c..f966289 100644
--- a/test/classes/Plugins/Export/ExportOdtTest.php
+++ b/test/classes/Plugins/Export/ExportOdtTest.php
@@ -218,10 +218,17 @@ class ExportOdtTest extends AbstractTestCase
     {
         $GLOBALS['odt_buffer'] = 'header';
 
-        $this->expectOutputRegex('/^504b.*636f6e74656e742e786d6c/');
-        $this->setOutputCallback('bin2hex');
+        \ob_start();
+        $this->assertTrue(
+            $this->object->exportFooter()
+        );
+        $output = \ob_get_contents();
+        \ob_end_clean();
 
-        self::assertTrue($this->object->exportFooter());
+        $this->assertStringContainsString('content.xml', $output);
+        $output = bin2hex($output);
+        // ODT, ODS, ... file header
+        $this->assertSame('504b0304', substr($output, 0, 8));
 
         self::assertStringContainsString('header', $GLOBALS['odt_buffer']);
 
